WAP: Wireless access point. 
- an AP acts as a central transmitter and reciever of wireless signals. 
- an AP can either be a stand-alone device or router
- Fat or thicc AP have 802.11 security, management and perf functionality built in . 
- Thin AP minimize device intelligence and offload config features and management to associated controller. 

SSID: Set Service ID
- the code that ID's membership with a WAP
- all wireless devices that want to communicate on a network must have their SSID set to the 
same value as the WAP SSID to est. connection with the WAP. 
- by default a WAP will broadcast its SSID ever few seconds. 
- you can stop the broadcast to that an attacker will not auto discover the SSID
-- the SSID is still included in packet frames so sniffing will allow an attacker to discover the SSID value. 

MAC filtering: 
- specify MAC of devices that are allowed / denied to connect. 
- frames can be sniffed to discover a valid MAC address [for MAC spoofing.]

