NAC: Network Access Control: 
- govenrs connections to the network based on configuration requirements 

DLP: Data Loss Prevention: 
- inspects content with the objective of preventing data exfiltration. Either intended or unintended. 

SEG: Secure Email Gateway: 
- Antivirus altimalware, antiphishing, and antispam features, in addtion to DLP and encryption enforcement. 

NAC: 
- agent or agentless approach to network security that attempts to unify endpoint security, user or system 
auth and network security enforcement. 
- agent is code that performs a function on behalf of the application. 

NAC Agents: 
persistent or permanent: installed on a device and runs continuously 
dissolvable or portal-based: downloads and runs when required, then dissapears. 
agentless: integrates with directory services. 

NAC Policies: 
preadmission policies: determine if a device is allowed on the network, and if so what segment based on host
health. 
- anti-virus application level 
- OS and application patch level 
- firewall and hos intrusion prevention software status 
- config settings 

NAC post admission policies regulate and restrice access once the connection is allowed. 

NAC Remediation: 
quarentine network: restricted IP network that provides users with routed access only to certain hosts, 
and applications that can provide the appropriate update. 

captive portal: redirects users to a web applicatoin that provides instructions and tools for updating thier
computer. 

DLP: 
- detect and prevent data exfiltration [un-auth;ed release or removal of data]
- DLPs locate and catalouge sensitive data, based on a set of rules 
- DLPs monitor target data while in use, in motion, and at rest. 

DLP Locations: 
network based / on-prem: network based hardware or VM that deals with data in motion and is usuall located
on the network perimeter. 
storage based: software that operates on long-term storage. 
end-point based: software that operates on a local device and focuses on data-in-use
cloud based: cloud based that operates in the cloud data in use, motion, and at rest. 

DLP Endpoint port blocking: 
- ID removable devices or media connected to the network 
- control and manage removable devices through endpoint ports, like USB, FireWire, Wi-Fi, Modem / NIC and 
Bluetooth. 
- require encryption, limit file types, limit file size
- provide detailed forensics on device usage, data transfer by person time, file type, and ammount. 

SEG
- inbound filtering of phishing emails, SPAM, or malicious emails.
- outbound data loss prevention and email encryption. 

SEG Locations: 
- email server
- on network hardware or VM 
- cloud based 

SEG Features: 
- quarintine or block emails 
- prevent data leakage
- force email encryption 
- sandboxing: use an env to test a file 
- threat intel 
