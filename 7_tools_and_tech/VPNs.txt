Virtual Private Network: 

Remote Access Applications: 
Telnet - Terminal Emulation: Port 23: 
- facilitates a connection to a remote system and the execution of commands. 
- provides basic auth with username:password
- communication is in clear text. 

SSH - Secure Shell Terminal Emulation: Port 22: 
- facilitates a connection to a remote system and the execution of commands.
- commands are sent over a secure tunnel 

Remote Desktop Software: RDP: Port 3389
- software or OS feature that allows a desktop env to be run remotely

VPN: 
- Secure priviate connectoin between two endpoints
- can be: host-to-host, host-to-network, or network-to-network
- designed to facilitate remote access over a public network. 
- cost effective alternative to point to point connections by transforming the internet into a secure circuit. 
- VPSs isolate network frames from the surrounding network using tunneling 
- full tunneling: all traffic is routed over the VPN
- split tunneling allows the routing of some traffic over the VPN wile letting other traffic directly access 
the internet

VPN protocols: 
PPTP: MSFT's impl of secure communicatoin over VPN
- designed fro point to point (PPP)
- no longer secure

L2TP: Cisco's impl of secure communication over VPN
- combines layer 2 forwarding and PPTP
- can be used on IP and non-IP networks

SSL/TLS: for connections in a browser. 
- User is connected to an SSL gateway or endpoints
- SSL VPN portal is a single connection to multi-services. 

IPsec: This is the defacto standard. 
- host to host, host to site, or site to site. 
- native to IPv6 
- bolt-on to IPv4 
- uses crypto to provide auth, integrity, confidenitiality, and non-repudiation. 

Ipsec Modes: 
- transport: used for end to end protection between a client and server. 
- the IP payload is encrypted
- transport is the default mode for IPsec. 

tunnel: used between server to server, server to gateway or gateway to gateway
- the entier packet is encrypted. 

IPsec Protocols: 
There are two distinct protocols: 
- Auth Header [AH]: provides a mechanism for auth only. 
-- provides data integrity, date origin auth, and optional replay protection service. 

- Encapsulating Security Payload [ESP]: 
-- provides data confidenitiality/encryption and data integrity, date origin auth, 
and optional replay protection service. 
-- can be used with confidenitiality only, auth only, or both. 

IPsec SPI: 
- a negociated security association [SA] includes the algorithms that will be used [hashing and encryption]
key length and key info
- security associations are id'ed by a security parameter index [SPI]
- two seperate SA's are established for each direction of data communication

SSL/TLS VPN: 
- SSL/TLS portal VPN: single connection for accessing multiple services on a web server. 
-- a page acts as a portal to other services. 

- SSL/TLS VPN: used to access a server that is not a webserver
-- uses custom programming to provide access through a web browser. 

Always on VPN: 
- this is new
- connection and auth are transparent to the user

