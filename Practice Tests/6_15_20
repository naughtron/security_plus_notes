Q: What should a disaster recovery plan (DRP) contain?
A: A disaster recovery plan should contain (among other things) a list of critical systems in order from the most critical to the least critical.

Q: What are kernel-level rootkits designed to do to a computer?
A: Rootkits in general are designed to gain administrator access while not being detected. Kernel-level rootkits will change code within the operating system and possibly device drivers, enabling the attacker to execute with the same privileges as the operating system. This type of rootkit allows for unrestricted security access.

Q: You are surprised to notice that a co-worker’s computer is communicating with an unknown IRC server and is scanning other systems on the network. None of this was scheduled by anyone in your organization, and the user appears to be unaware of what is transpiring. What is the most likely cause?
A:  If the computer in question is scanning the network and accessing an unknown IRC server without the user’s knowledge, then the computer has probably been compromised as a zombie and is now part of a botnet. The IRC server probably acts as a central communication point for all zombies in the botnet.

Q: Hardware-based encryption devices such as hardware security modules (HSMs) are sometimes deployed by organizations more slowly than in other organizations. What is the best reason for this?
A: A lack of management software can cause slower deployment of HSMs.

Q: Which of the following web application security weaknesses can be mitigated by preventing the usage of HTML tags?
A: Cross-site scripting (XSS) is an attack on website applications that injects client-side script into web pages.

Q: A malicious computer is sending data frames with false hardware addresses to a switch. What is happening?
A: ARP poisoning is an attack that exploits Ethernet networks—spoofed frames of data will contain false MAC addresses, ultimately sending false hardware address updates to a switch.

Q: Which port does Kerberos use by default?
A: Kerberos uses inbound port 88 by default. An example of this would be a Microsoft domain controller that accepts incoming logins. Kerberos is a type of mutual authentication.

Q: Which of the following is used to cache content?
A: Proxy

Q: A systems administrator requires an all-in-one device that combines various levels of defense into one solution. She requires a single device that sits last on the network before the Internet connection. Which of the following would be the best solution?
A: A unified threat management (UTM) device is an all-in-one device that combines the various levels of defense into one solution. Often, this is a single device that sits last on the network before the Internet connection.

Q: Jennifer has been tasked with configuring multiple computers on the WLAN to use RDP on the same wireless router. Which of the following might be necessary to implement?
A: If there are multiple computers allowing incoming Remote Desktop Protocol (RDP) sessions on the WLAN, you might have to configure the wireless router to forward each computer to a different RDP port.

Q: What two security precautions can best help to protect against wireless network attacks?
A: Authentication and WPA

Q: Which of the following is a secure wireless authentication method that uses a RADIUS server for the authenticating?
A: LEAP (Lightweight Extensible Authentication Protocol) is Cisco’s version of EAP. It allows for dynamic Wired Equivalent Privacy (WEP) keys and mutual authentication with a RADIUS server.

Q: Your boss asks you to replace the current RADIUS authentication system with a more secure system. Your current RADIUS solution supports EAP, and your new solution should do the same. Which of the following is the best option and would offer the easiest transition?
A: The Diameter protocol is, like RADIUS, another AAA protocol, but is a more evolved protocol and utilizes more reliable transport mechanisms such as TCP and Stream Control Transmission Protocol (SCTP), as opposed to UDP. Like RADIUS, many Diameter applications allow for the use of the Extensible Authentication Protocol (EAP).

Q: You have been tasked with providing a staff of 250 employees secure remote access to your corporate network. Which of the following is the best solution?
A: The VPN concentrator is the best solution listed. A hardware device such as this can handle 250 concurrent, secure, remote connections to the network.

Q: Your network is a Windows domain controlled by a Windows Server domain controller. Your goal is to configure user access to file folders shared to the network. In your organization, directory access is dependent upon a user’s role in the organization. You need to keep to a minimum the administrative overhead needed to manage access security. You need to be able to quickly modify a user’s permissions if that user is assigned to a different role. A user can be assigned to more than one role within the organization. What solutions should you implement? (Select the two best answers.)
A: Create security groups and assign access permissions based on organizational roles
Create an OU for each organizational role and link GPOs to each OU

Q: You have been commissioned by a customer to implement a network access control model that limits remote users’ network usage to normal business hours only. You create one policy that applies to all the remote users. What access control model are you implementing?
A: Role-based access control, because it is for REMOTE USERS.

Q: Your boss needs you to implement a password policy that prevents a user from reusing the same password. To be effective, the policy must be implemented in conjunction with the password history policy. Which of the following is the best method?
A: This question refers to Windows Server products. The minimum age password policy setting must be set to enforce an effective password history policy. If this is not done (in conjunction with the password history policy), then the user will be able to reuse old passwords.

Q: Identifying residual risk is considered to be the most important task when dealing with which of the following?
A: Risk acceptance, also known as risk retention, is the amount of risk that an organization is willing to allow after risk has been reduced as much as possible. The actual amount is known as residual risk; the amount left over after a detailed security plan has been implemented.

Q: A security assessment of an existing application has never been made. Which of the following is the best assessment technique to use to identify an application’s security posture?
A: Baseline reporting is the best answer for identifying the application’s security posture. A Security Posture Assessment (SPA) is used to find out the baseline security of an application, a system, or a network, as long as the application (or system or network) already exists.

Q: You need to protect passwords. Which of the following protocols is not recommended because it can supply passwords over the network?
A: SNMP

Q: Which of the following encryption algorithms are supported by the IEEE 802.11i standard?
A: TKIP and AES

Q: Alice wishes to send a file to Bob using a PKI. Which of the following types of keys should Alice use to sign the file?
A: Alice’s private key

Q: Your organization has a policy that states that user passwords must be at least 16 characters. Your computers use NTLM2 authentication for clients. Which of the following hash algorithms will be used for password authentication?
A: The MD5 hashing algorithm is used by NTLM2 authentication.

Q: Your web server’s private key has been compromised by a malicious intruder. What, as the security administrator, should you do?
A: Submit the public key to the CRL.

Q: You are contracted with a customer to protect its user data. The customer requires the following:
Easy backup of all user data
Minimizing the risk of physical data theft
Minimizing the impact of failure on any one file server
Which of the following solutions should you implement?

A: Use file servers with removable hard disks. Secure the hard disks in a separate area after hours.

Q: The helpdesk department for your organization reports that there are increased calls from clients reporting malware-infected computers. Which of the following steps of incident response is the most appropriate as a first response?
A: Identification