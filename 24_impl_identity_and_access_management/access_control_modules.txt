authorization is the process of granting users and systems (subjects) access to resources (objects)

Access Control Model:
- this is a framework that dictates how subjects access objects or
how objects access objects
- Access Control Modles are built into some OS' and some applications.

Access control Models and Techniques:

Subject Based Models:
- Mandatory Access Control (MAC):
-- access is based on the relationship between subject clearance and need
to know and the object classification level
-- enforcemtn: Security Labels

MAC Security Lable Matrix:
- Object File A:
-- classification: top secret
-- subject clearance: top secret
-- need to know: TBD

- Object File B:
-- classification secret
-- subject clearance: top secret or secret
-- need to know: TBD

- Object File C:
- classification confidential
- subject clearance: top secret, secret, or confidential
- need to know: TBD

- Discretionary Access Control (DAC):
-- Data owners decide subject access
-- enforced with access control lists
-- enforced with capibilities tables

- The owner can assign permissions
-- owner by default has permissions to all files they create
-- they can assign permissions to all files
-- permissions can also be assigned to a group

DAC Access Permissions:
- user permissions are assigned to a user account
- group permissions are assigned to each member of the group
- inhereted permissions refer to permissions that flow from parent to child dirs and files
- cumulative permissions are combination of all user and group assigned and inhereted permissions
- the exception is that explicit deny permission this will overrule ALL other assigned permissions.

- Role-Based Access Control (RBAC):
-- access is based on subjects assigned Role
-- many-to-many relationship allowed
-- enforced with Access control lists
-- enforced with Capabilities Tables
-- enforced with Security Policies

RBAC ACL Matrix:
- users are assigned to a role of roles
- those roles are given permissions
- multiple role permissions are cumulitive
- higher permission between read and full control is full control

Object-Based models:
- Rule Based Access Control:
-- access based on situational if-then statements
-- enforced by rules

- Content Based Access Controls:
-- filter based on the data being acted upon
--enforced by keywords, and categories

- Context Based Access Control:
-- access based on collection or sequence of actions
-- enforced with rules and security policy

- Constrained interfaces:
-- access restricted by functionality
-- menu, or shell, or database views
-- enforced by design and configuration

ABAC:
- Attribute based access control
- logical access control model that controls access to objects by evaluating
rules against the attributes of both subject and object
- ABAC supports a complex bool rules that can eval different attributes
- the policies that can be impl in ABAC are limited only to the degree imposed
by the computational language and the richness of the available attributes
- an example that is consistent with ABAC is the Extensible Access Control Markup Lang

