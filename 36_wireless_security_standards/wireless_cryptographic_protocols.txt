WPAN: Wireless Personal Area Network aka Bluetooth.
- 802.15 standard to connect devices within a limited range.

WLAN: Wireless local area network.
- 802.11 standard

WMAN: Wireless Metropolitan Area Network:
- 802.16 standard

WWAN: Wireless Wide Area Network:
- Point to point microwave links

802.11 IEEE Standards:

802.11:
- 2mbps data rate
- 2.4 ghz freq
- 100m distance

802.11b:
- 11mbps data rate
- 2.4 ghz freq
- 140m distance

802.11a:
- 54 mbps data rate
- 5 ghz freq
- 120m

802.11g:
- 54 mbps
- 2.4 ghz freq
- 140m distance

802.11.n:
- 150 mbps
- 2.4/5 ghz greq
- 250m distance

802.11i:
- Security for 802.11 technologies

802.11e Quality of Service for priotity and time sensitive data.

Wireless Authentication Protocols:

EAP:
Extensible Authentication Protocol:
- EAP is a authentication framework for wireless networks and P2P connectoins.

PEAP:
Protected Extensible Authentication Protocol
- Encapsulates EAP in a TLS tunnel
- Jointly developed by MSFT and RSA
From practice exam:
PEAP uses MSCHAPv2 most commonly. This supports authentication via
Microsoft Active Directory databases.

EAP-FAST:
EAP - Flexible Authentication via Secure Tunneling
- Lightweight impl proposed by Cisco.

EAP-TLS:
EAP transport layer security:
- defined in RFC 5216 is an open standard that uses TLS
- considered one of the most secure EAP standards.
From Practice exam:
EAP-TLS uses Transport Layer Security, which is a certificate-based system that does enable mutual
authentication. This does not work well in enterprise scenarios because certificates must be
configured or managed on the client side and server side.

EAP-TTLS:
EAP Tunneled Transport Layer Security
- Extends TLS
- The client does ont need to have a cert
- legacy authentication is supported with secure tunnel

