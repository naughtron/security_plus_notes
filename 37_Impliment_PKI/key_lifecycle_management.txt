Crypto System Strength:
- While many components of PKI are publicly known, private and symmetric keys
must be appropriately strong.
- The strength of a key should be commensurate with the dara/process protection requirements.

The longer the key the stronger it is, and the more processing power is requireed.

Key Management:
- This described the activities involving the handling or cryptographic keys and other related seciryt paramters
like passwords during the entier lifecycle.
- this includes the generation, exchange, storage, use, crypto-shredding, and replacement of keys
- Key Management Practices Statement KMPS is a document that describes in detail the organizational structure responsible roles, and rules for
key managment.

Key useage best practices:
- Keys should only have ONE purpose.
- The use of the same lkey for different cryptographic purposes may weaken the security provided by one or both keys
- Limiting the use of a key limits potential compromise and damage.

Key Update best practices.
- Keys should be frequently changed.
- This also helps to limit loss of information as the number of messages encrypted with a key will decrease as the keys change.

Perfect Forward Secrecy:
PFS: employed to ensure that if a private key was compromised all previous uses of the key would remain secure.
PFS is achived by using temporary key pairs to secure each session, they are generated as needed held in RAM during the session and then
disgarded after use.

Key Storage:
Private keys must be stored securly.
The mesures taken to protect a private key must be at least equal to the required security that the key is used for.
- Centralized Storage: refers to the use of a key managment server (org control)
- Decentralized Storage: system of local key storage and management (user control)

Hardware Security Model HSM:
- This is used to store cryptographic keys in tamper resistant hardware providing logical and physical protection.
- This can also be used to generate keys.
- These modules traditionally come in the form of a plug-in card or external device that attaches to a computer or server.

Key Escrow:
- a proactive agreement in which the keys needed to decrypt encrypted data are held in Escrow
by a third party so under certain circumstances an authorized third party may gain access to the keys.

Key Descrution:
- All copies of a private key or symmetric keys should be irrevocably destroyed as soon as they are no longer required.
- this is done to minimize the risk of a compromise.
