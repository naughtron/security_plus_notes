This is the method of circumventing the security of a cryptographic system by finding a weakness in a code, cipher, cryptographic protocol or key managmeent scheme.
- this is reffered to as "cryptanalysis".

Crypto Attack Categories:
The intent is to break a crypto system and find the palintext from the ciphertext.
The attackers objective is to id the key.

Known Ciphertext: a sample of ciphertext is available without the plaintext associated with it.

Known PlaintexT: a sample of ciphertext and the corresponding known plaintext is available.

Chosen Plaintext: can choose the plaintext to get encrypted and obtain the corresponding ciphertext

Chosen Ciphertext: can select the ciphertext and obtain the corresponding plaintext.

In all of htese cases they just want to ID the key.

Key Attacks:
Brute Force: every possible key is tested can be done both online and offline.

Dict: list of knwon keys are tested.

Frequency: looking for patterns to reveal the key

Replay: attacker tries to repeat or delay a cryptographic transmission.

Hash Attacks:
Collision: Using brute force to find two inputs producing the same hash value

Birthday: exploits the mathematics behind the birthday problem in probability theroy to cause collision.
- how many people must be in the same room for the chance to be greater than even that another person has the same birthday as you
= 253
- how many people need to be in the room for the chance to be greater than even at least two people share a birhtday
= 23
- any random set of 23 >= 50% chance

Rainbow Table: comparing a table of known inputs and outputs to unknown outputs.

Downgrade Attack:
- Force degridation to a low cryptomode if available designed for backward compatibility
- 2014 POODLE attack took advantage of the option resulting in a client fallback to SSL 3.0

Weak Implimentation:
- Attacker takes advantage of misconfigurations, weak keys, broken or depricated versions.
- Depricated means that the use of the algorithm and key length is allowed but the user must accecpt some risk from weakness.
- Broken means that the algorithm or key length is exploitable.

DROWN: Decrypting RSA with Obsolete and Weakended encryption.
https://nvd.nist.gov/vuln/detail/CVE-2016-0800
