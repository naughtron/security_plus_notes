SaaS: Software as a service. 
- provided: compute resources, OS, and application 
- no managment or control of the application 
- considerations: availability, maintiance, vuln managment, privacy, data ownership,
multitenacy, testing. 

PaaS: Platform as a service: 
- provided: compute resources, OS, perhaps a Db. You deploy what you created. 
- customer does not manage the underlying infastructure
- considerations: availability, maintiance, vuln management, provided, data ownership

IaaS: infastructure as a service: 
- you are provided compute resources only. 
- customer has control over the OS, storage, and deployment
- considerations: availability, maintiance, vuln management

Cloud Deployment Models: 

Public Cloud: provisioned for public use
considerations: location multitennancy

Community Cloud: provisioned for the exclusive use by well defined group
considerations: multitenacy

Private Cloud: provisioned for the exclusive use of a single group
considerations: scalability 

Hybrind Cloud: the public and private infastructure opperate independent of eachother
considerations: security of the connection. 

CASB: Cloud Access Security Brokers
- security policies are interjected as cloud-based resources are accessed. 
-- examples: auth, encryption, visibility, Data Loss Prevention
- provides control over "shadow IT" applications. 
- they proxy traffic to ID cloud applications. 

Security As a Service: SecaaS: 
- managed security services for: public, private, and hybrid cloud envs
- takes the burden off the Sa* providers for sec protection and enforcement. 
- services include: encryption, activity monitoring, DLP, malware detection, filtering, firewall, policy enforcement, 
email security, intrusion detection, and auth. 

on prem vs could: 
Legal regulation / contract: 
- need to eval language, regulatory or contractual restrictions or obligations. 
- country where the data is stored of processed. 
- required controls in place

infastructure: need to eval the required investment to build, maintain, and support 

availability: need to eval capibility of resources and ability to provision additional resources

DDR & BR: need to eval disaster recovery, and business continetuity

Security: need to eval security and privacy requirements




