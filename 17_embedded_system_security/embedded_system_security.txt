embedded system: 
- electronic product that contains a micro-processor and software designed to perform a specific task. 
- these systems are found in consumer electronics, industrial, auto, med, and mil. 
- these systems range from personal devices to industrial control systems. 

Embedded System Components: 
- embedded system usually has three closed loop components. 
- System on a chip: SoC: microprocessor or controller with advanced peripherals
- Real Time OS RTOS: defines the way the system works
- Application software specific to the device. 

Vulns: 
- devices are designed for functionality and convience not security 
- devices are powered by special chips that are inexpensive and profit margins are slim
- strong incentive to use open source software 
- that software is often out of date. 
- little incentive to maintain

Types of Attacks: 
Network Attack: exploit the protocol or impl [for example open ports]
- injection 
- priv esc. 
- packet capture

Active Side Channel: using a voltage glitch on the PSU to cause a program malfunction

Memory and Bus: Physically connect to the hardware and read contents of the memory. 

Weak Auth: using the known default admin password to gain access. 

Botnet: weponized devices for use in DDoS attacks. 

Stepping Stone: Embedded devices such as HVAC systems and camera systems that often connect to a network. 

Industrial Control Systems: 
- ICS these are embedded sytems that monitor and control things that exist in the real world. 
- SCADA: systems that monitor and control entier sites. Gas, electric, water, waster....
- Top threats include policially motovated attack and human error
- Challenges are: weak auth, minimal visibliity, outdated OS' and inability to patch 

This is becoming a more and more serious issue, esp with IoT. 
- wearables 
- home automation 

Best practices: 
- research the supply chain including chip manufactureer and software
- research available security controls 
- change the default creds
- use strong passwords / multi-factor if availabel 
- disable features that are not needed. 
- disable telnet, and use ssh when possible
- segregate the divices whenever possible 
- disable or protect remote access to the device when not needed. 
- always check for firmware updates. 