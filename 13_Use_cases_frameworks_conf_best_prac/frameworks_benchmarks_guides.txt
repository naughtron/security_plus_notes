Framework: a logical structure 
- The intent of a info sec framework is to dockument and organize process
related to impl, management, and maintaince of info sec controls. 
- A framework can be the basis of a common assessment and cert. 
- used by stakholders to eval controls. 
- external audits to eval and assess controls that are in place 
- third parties to eval the risk of partnering with/doing business with an org. 

Framework Focus: 
Industry Standard: based on established and generally accecpted norms. 
Regulatory: reflective of regulatory expectations and examination of reqs
National: designed for use in a specific country or consortium objective is generally econ or military sec. 
International: Promoted Globally, Developed by reps from particapating areas. 
Industry Specific: Specific to an industry, developed, promoted, and enforced by industry members. 

Infosec Sec Frameworks: 
International: ISO 27000 Family
National: NIST Cyber Sec Framework 
Regulatory: FDIC Cyber Sec Assessment Tool (CAT)
Insudtry Specific: HITRUST, PCI DSS Payment Card Industry Data Security Standard. 

Benchmark: Intended to help an org id their cyber sec capibilities and comp those efforts to peers 
- This is either done by a third party of within the org. 

Configuratoin Guidance: Platform and vendor specific config guidance is availabel from a varity of sources. 
- guidance can be in the form of narrative, checklist, spreadsheet or install files. 
- no need to reinvent the wheel 
- config changes should be subject to config management and change control process. 
- NIST SP800 Publications 
- NIST checklist program repo

Defence in Depth: in addition to multiple layers of controls. 
- vendor diversity
- control diversity 
- user training 

