cryptography:
- science of writing that enables an entity to
store data, transmit data, and process data in a
form that is only available to an intended recipient.

store == data at rest
transmit == data in transmit
process == data in use

Use Cases:
confidentiality: encryption
integrity: hashing
non-repudiation: digital signature
authentication: digital certificate
obfustication: encryption, steganography

Terminology:
Plaintext/cleartext: human readable text
Ciphertext: encrypted and/or human unreadable text
Cipher: a technique that transforms plaintext into ciphertext and back to cleartext
Algorithm: a cryptographic alogrithim is mathmatically complex modern ciphertext
Stream Cipher: alogrithm that works with one bit at a time
Block Cipher: Algorithm that works with blocks of data

Terminology - Keys:
Key/Cryptovariable: Secret value used with an alogrithim
- the key ditates what parts of the alogrithim will be used, in what order
and with what values.

Key Space: Number of possible key combonations
example: 256-bit == 1.1578 x 1077 possible Keys

Key Stretching: technique to strengthen a weak key and protect against brute force attacks
this feeds the inital key into an alogrithim bcrypt for example, and that outputs an enhanced key

Symmetric: Using a single key

Asymetric: Using two mathematically related keys [public and private]

Ephemeral Key: Temporary key generated for a connection and never used again.

Cipher Techniques:
Substitution Cipher: replaces one char or bit for anohter char or bit
The key is the shift pattern.
ROT13 for example

Transport Cipher: moves chars or bits to anohter place withing the message
block.
The key is the transportation code.

Confusion: process of changing the values.
Complex substitution functions are used to create confusion.

Diffusion: process of changing the order.
Sending bits through multiple reounds of transportation, this is used to create diffusion.

Lightweight cryptography:
cryptography that runs on low power, low latency, high resiliency requirements.

Strength and Workfactor:
Strenght: combo of the alogrithim + algorithmic process + length of key + secrecy of key.
If one element is weak, the system can potentially be compromised.

Work Factor: thie amount of time and effort it would take to break the crypto system

Depricated: the use of the algorithm and key length is allowed but the user must accecpt
some risk due to weakness.

Broken: the algorithm or key length is exploitable.

THE STRONGER THE CRYPTO SYSTEM THE MORE PROCESSING POWER IS REQUIRED

Security through obscurity:
This is a bad idea.

