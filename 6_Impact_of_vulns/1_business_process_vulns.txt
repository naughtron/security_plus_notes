- Unessessary Risk
- Financial Loss
- Reputational damage
- Inefficient operations
- Low Moral
- Regulatory non-compliance

Configuration Vulns: 
- are often a reflection of weak or non-existant business process including configuration, change, 
and vuln management. 
- Consequences: 
- Exposure [oppertunistic target]
- Theft and Loss
- Interruption of service/downtime
- Resource Exaustion 
- Compromise 

Assesset Related Vulns: 
- System Sprawl: this is unplanned or unmanaged growth, insonsistent platforms, multi-vendors
-- Impact: Support burden, decentralized management, can't enforce standards

- Architect / Design Weakness: Implimentation of inherently insecure systems or network segments
-- Impact: exposure to attack 

- Undocumented assets: assets not inventoried, documented or tracked. 
-- Impact: Excluded from vuln, change and config management process. 
-- not included in testing scope
-- loss of ownership control

Vendor Related Vulns: 
- End of life System: devices, OS' or applications that are no longer supported by the vendors
-- Impact: Vulns not addressed, no vendor support or guidance 

- Lack of Vendor Support: Absence of support or maintainance contracts deficient warranty unresponsive vendor
-- Impact: VUln not addressed, problems unresolved, downtime. 

Personnel Related Vulns: 
- Untrained User: Users who are not aware of / do not understand their role in protecting
information and info systems. 
-- Impact: Inadvertant participation in exploits, bypassing controls, inability to see or report incidents. 

- Improperly configured accounts: Accounts that have incorrect rights/ permissions explicit or in a group
Accounts that are stale. 
-- Impact: Inadvertant participation in exploits. ability to make adverse changes, confidentiality violations. 

Configuration Related Vulns: 
- Default Configurations: using out of the box settings that favor conviencence over security 
-- Impact: dimished attack workfactor as default settings are publicly known

- Misconfiguration: Configuration Params either incorrect or not set in accordance with best practices. 
-- Impact: Could be significant depending upon what the misconfig is. 

- Resource Exaustion: REsources required to execute an action are entirely expended
-- Impact: DoS

- Embedded Systems: Proprietary configurations / old software with inability to update. 
-- Impact: Unknown exposure, vulns not addressed. 

Crypto Related Vulns:
- Weak Ciphers: Implimentation ciphers that are either broken, depricated, weakm or not on par with the
level of protection that is required. 
-- Impact: Unexpected exposure and confidentiality violations. 

- Improper Cert and Key management: not properly protecting private keys, not properly managing Cert ownership. 
-- Impact: Unauth'ed access or impersonation and or reputation damage

Vuln Management: 
- define your state of security: where do we want to be where do we need to be 
- create standards and baselines 
- assess vulns 
- prioritize vulns
- mitigate vulns
- monitor your env / org / business. 

