availability: 
- mesurment of a systems uptime being actually operational and providing intended service. 
- five nines. 
- resiliency is the capibility to continue operation even when there has been a disruption. 

Single Point Of Failure (SPOF): 
- redundency is a duplication of critical components or functions with the intent to increase reliability
and mitigate the risk associated with a single point of failure. 

- redundancy can be configured to require manual intervention or be automatic. 

Fault Tolerance: 
- capibility of a system to continue to operate in the event of a failure of one or more systems / components. 
- rely on reduandnt components. 
- continious monitoring. 
- failures are transparent to users. 

Active vs. Passive redundancy
- passive components are inactive until failure occurs. 
- active components operate in paralell with the primary system to provide service without noticable interruption. 

RAID: 
0: data is written across multiple disks: 
- objective: performance only

1: data is mirrored on two identical drives: 
- objective: fault tolerance 

5: data is written across three or more drives with one parity stripe so it can revocer two drives 
- objective: fault tolerance

10 (aka 1+0): data is simultaneously mirrored and striped across several drives: 
- use a min of 4 drives. 
- objective: fault tolerance, and performance

Swapping: 
- process of replacing a failed componet 
- warm swap: ability to insert and remove hardware while the system is suspended. 
- hot swap: ability to insert and remove hardware while the system is running. 
- hot plug: ability to add a component without interruption. 

High Availability (HA)
asymmetric: acvite/passive primary and standby systems. when a fault occurs the stand-by device takes over
symmetric: active/active two primary systems each machine monitors the other and takes over when a failure occurs. 

