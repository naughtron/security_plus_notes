objective: 
- to id vulns in the target env and report potential exposure including 
rogue devices. 
- in general considered a passive activity
- general interal acivity that should be done on a regular basis
- scanning with auth 
- scanning without auth

Non-intrusive scanning applications: 
- inspect file system 
- inspect registry
- inspect patches
- reports vulns, but will not act on them 
- reports misconfigurations 
- do not take action to resolve problems 

Intrusive Vuln Scanner
- probe devices, examine responses, and validate vulns
- can be used to simulate an attack 
- can take action to resolve issues 

scanning credentials non-cred/auth: 
- no authentication 
- easier, quicker, less system resources, inexpensive
- disadvantage: incomplete findings 

scanning with auth: 
- account is auth'ed to simulate a user or admin on the system 
- comprehensive 
- disadvantage: requires configured accounts
- disadvantage: time consuming 
- disadvantage: resource intense 
- disadvantage: may result in unintended conquiences including 
DoS or high volume alerts

Scan Reporting: 
- Port Scanner: ID open ports and fingerprint OS
-- nmap and superscan

- Network Map: Map of devices and applications 
-- nmap and spiceworks

- Vuln Analyzer: ID published vulns 
-- OpenVAS
-- Nessus

- Configuration Analyzer: 
-- ID config errors or lack of controls: 
-- PEAS, LinEnum, Lynis, MBSA

IMPORTANT: 
False Positive: Normal is incorrectly ID'ed as abnormal
Fasle Negative: Abnormal is incorrectly ID'ed as normal - far more dangerous

best practices: 
- create an inventory of network assets 
- ID critical assets
- determine freq of scanning
- confirm scope
- make sure your scanner is up to disadvantage
- document timelines and thresholds. 

