common examples: 
- unpatched operating systems and applications
- broken versions 
- weak ciphers 
- inproper input / output handling

Exposure: 
- an exposure is a system or software configuration issue or 
a lack of a control that could contribute to a successful exploit or
compromise. 
- common examples: 
- default settings
- running unessessary services that are common attack points: HTTP, FTP, SMTP
- weak passwords
- end of life applications or devices. 
- system sprawl: too many systems / devices 

Common Vuln and Exposures (CVE)