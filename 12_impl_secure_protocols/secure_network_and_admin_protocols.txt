NTP: Network Time protocol
- network protocol for clock sync. 
- used to sync all participating computers to within ms of a Coordinated Universal Time {UTC}
- Current version is NTP v4
- NTP is not considered secure. 
- UDP on port 123
- used for time sync. 

LDAP/LDAPS: Lightweight Directory Access Protocol: 
- Directory Service that is centralized and distributed database of user data, computers, and trused entities. 
- AD is MSFT impl that works in conjuction with kerberos on port TCP 389
LDAPS is used to protect LDAP credentials for non-MSFT clients
- LDAPS is on port 636
- used to manage objects, users, auth, permissions, and trusted relationships. 

SSH: Secure Shell: 
- est a secure secure connection between a client and server. 
- runs on port 22
- replacement for cleartext shells like telnet, rlogin, rsh, rsync
- use for remote access and admin. 

SRTP: Secure Real-time Transport Protocol: 
- used for enhanced security for VoIP, audio, and video. 
- uses encryption and auth 
- uses UDP port 5004

SNMP: Secure Network Management Protocol 
- used to manage and monitor IP devices. 
- SNMP is supported on modems, routers, switches, servers, workstations, and printers.
- components include managed devices, agents, and management stations. 
- Version3 SMNPv3 added crypto securtiy features and replacement for cleartext versions in V1 and V2
- Agents listen on UDP port 161
- Managers recieve notificatoins on UDP port 162

DHCP/82: Dynamic Host Configuration Protocol Option 82: 
- used to dynamically assign IP addresses and other params 
- DHCP itself has no client/server auth 
- Option 82 provides info about the client trying to connect and the DHCP server used that info to assign an IP or params
- ISP use this alot to only allow subscribers to connect. 
- DHCP clients listen on UDP 67
- DHCP Servers lisen on port 68

DNSSec: Domain Name System Security 
- used to resolved domain names to IP addresses. 
- DNSSec signs the zone data 
- DNSSec needs to be deployed at EACH STEP in the lookup from root zone to final domain name. 
- DNS uses TCP port 53
