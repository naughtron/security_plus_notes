Output and Reporting:
- POC: Predictor of Compromise
- IOC: Indicator of Compromise

-- host security reporting --

HIDS/HIPS:
- purpose: host intrusion detection and prevention
- output: local suspicious activity response taken

Antivirus:
- purpose: block malicious code and scan for signatures
- output: malicious files Id'ed, blocked, quarentined, deleted.
- make sure that allowed traffic is being captured.

File Integrity Checker:
- purpose: id changes in files or file structure
- output: file changes including time, date, and file size

Firewall:
- purpose: control ingress and egress traffic
- output: allowed and denied traffic (including attacks)

DEP Data Execution Prevention:
- purpose: monitors program memory use:
- output: denied and blocked exe's

- Note from practice exam:
-- A HIDS usually cannot detect network attacks, but a NIDS can.
-- A HIDS will definetly impact system performance because it uses host CPU/Memory


-- enterprise sec tech --

Application Whitelisting:
- purpose: explicitly specify allowed applications
- output: application access allowed/denied

RMC: Removable Media Control:
- purpose: control access to and use of removable media.
- output: media access allowed/denied

Advanced Malware tools:
- purpose: ID malicious code that could evade or subvert anti-virus software
- output: supicious or malicious files
- output: suspicious activity
- output: suspicious or malicious traffic

Patch Management Tools:
- purpose: inventory patches, ID missing patches, and manage deployment.
- output: Patch inventory
- output: Missing patches
- output: patch deployment schedule
- output: patching errors - this is critical

-- border sec tech --

UTM Inified Threat Management:
- purpose: multiple: network intrusion, detection/prevention, gateway Antivirus
- output: depends upon what the device is being used for
- output: ingress and egress

DLP Data Loss Prevention:
- purpose: prevent malicious and accidental data exfiltration
- output: allowed and denied activity / quarentined activity [could be queued]

WAP Web Application Firewall:
- purpose: filters, monitors, inspects and blocks HTTP traffic to and from a web application
- output: suspicious traffic and requests including SQL Injection and XSS