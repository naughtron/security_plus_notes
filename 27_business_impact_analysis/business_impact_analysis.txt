Business Impact Analysis BIA:
- used to ID the impact of a disruption of MISSION-ESSENTIAL services,
systems, and infastructure.
- Essential means that the absence of or disruption of services would result in
significant, irrecoverable, or irreparable harm to an org, employees, business partners
community, or country.

BIA is used by management to:
- understand org continuity requirements
- make investment decisions
- guide development of an incident response.

Fundamental BIA questions:
- what are the orgs essential business processes?
- what is the impact of a disruption?
- what are the related resources and dependencies including SPOF
- what are the process, system, and data recoverty requirements?

The outcome of BIA is a prioritized matrix of services, systems, and infastructure.

Business Impact Metrics
MTD/MTO: Max Tolerable Downtime/Max Tolerable Outage
- maximum time a process/service canm be unavailable without causing signifigant harm
to the business.
RTO: Recovery Time Objective "looking back"
- Ammount of time allocated for system recovery
- must be less than the max amount of time a system resource can be unavailable before
there is an unaccecptable impact on other system resources
PRO Recovery Point Objective: "lookingg forward"
- Accecptable data loss
- the point in time, prior to a disruption or system outage that data can be recovered.
MTBF: Mean Time Between Failures:
- mesure of reliability stated in hours
MTTR: Mean Time To Repair:
- average time to repair a failed component or device
