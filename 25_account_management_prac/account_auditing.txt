Auditing Concepts:

Account Auditing: is this a valid account?

Membership Auditing: Are membership assignments correct?

Permission Auditing: are the explicit/inhereted permissions correct?

Usage Auditing: is user activity and resource use within normal boundries?

Authorization Creep: the accumulation of access rights, permissions and priv over time

Recertification: management validates that rights and permissions assignement are correct and that they
adhear to internal policies and compliance requirements.
- also known as attestation or entitlements review

User Account Managment: The following should be audited:
- priv accounts
- service accounts
- user accounts and access permissions
- group membership and permissions
- user and admin access

Priv Account Managment:
- are the accounts stale?
- should any of the accounts be disabled?
- are the assignements correct?
- correct number of priv accounts? too many? too few?
- are any of the accounts being shared?
- is all activitiy being logged / monitored?

Service Account Management:
- is the account still required?
- are the rights and permissons correct?

User Account Managment more:
- what was the last login date?
- are any internal accounts stale?
- are externam accounts still required?
- are account settings correct?
- should any accounts be disabled?
- are expiration dates correct?
- is group membership correct?

Group Account Management:
- are members correct?
- are rights and permissions correct?

User Access and Usage:
- monitor and verify users access to applications and permissions
- usually performed by a business unit

Monitoring of user activity:
- reporting on failed or successful user activitiy:
- account login/logout
- account lockouts
- invalid passwords
- password changes
- login restriction violation
- invalid accounts

Monitoring admin activity:
- server or console logins
- user management activity
- group management activity
- computer management activity
