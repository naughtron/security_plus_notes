rights: ability of a subject to take an action
- rights canb be assigned to user, group accounts or profiles.

permissions: functions that a subject can perform on an object, file, or folder.
- access control lists are used to assign permissions
- permissions can be assigned to user accounts groups accounts or profiles
- permissions are cumulative
- permissions can be explicit or inhereted.
- permissions shouls be audited on a reg basis

priv: override capabilities, overrides rights and permissions
- admin or root account
- priv trumps rughts and permissions
- priv credential theft and priv escalation are two of the most serious attack types.
-- pass the hash attack: passing hashed account credentials on one computer and reuses them to authenticate
with another computer.

need to know: demonstrated reason for requireing access
- subject has a demonstrated and proven reason for access.
- least priv assign only the rights/permissions needed to perform a task
- once need to know is established, least priv should be enforced

least priv: assign the minimal rights and permissions needed to accomplish a task

default deny: any access or actoin not explicitly allowed - is forbidden
- any access or action that is not EXPLICITLY ALLOWED is forbidden

default allow: any access or action not explicitly denied - is allowed
- any access or action that is not EXPLICITLY FORBIDDEN then it is allowed

time/location restrictions: restrictiongs that are based upon time or physical / logical location
- time of day: used to deny or allow access or terminate a session
- location: used to deny or allow access from a spec location, or IP, or specific device.

seperation of duties: breaking a task into segments so no one subject is in complete control


dual control: require more than one subject or key to complete a task